﻿using System;

namespace Thesis.Models
{
    public class TestModel
    {
        public TestModel()
        {
            PropertyOne = Guid.NewGuid().ToString();
            PropertyTwo = Guid.NewGuid().ToString();
            PropertyThree = Guid.NewGuid().ToString();
            PropertyFour = Guid.NewGuid().ToString();
            PropertyFive = Guid.NewGuid().ToString();
            PropertySix = Guid.NewGuid().ToString();
        }

        public int ID { get; set; }
        public string PropertyOne { get; set; }
        public string PropertyTwo { get; set; }
        public string PropertyThree { get; set; }
        public string PropertyFour { get; set; }
        public string PropertyFive { get; set; }
        public string PropertySix { get; set; }
    }
}