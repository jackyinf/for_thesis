﻿var React = require('react');
var rd3 = require('react-d3');

// https://github.com/esbullington/react-d3
// http://esbullington.github.io/react-d3-website/

// Data
var lineData = [
    {
        name: "series1",
        values: [{ x: 0, y: 20 }, { x: 24, y: 10 }]
    },
    {
        name: "series2",
        values: [{ x: 70, y: 82 }, { x: 76, y: 82 }]
    }
];

var scatterData = [
    {
        name: "series1",
        values: [{ x: 0, y: 20 }, { x: 24, y: 10 }]
    },
    {
        name: "series3",
        values: [{ x: 70, y: 82 }, { x: 76, y: 82 }]
    }
];

var areaData = [
    {
        name: "series1",
        values: [{ x: new Date(), y: 20.5 }, { x: new Date(), y: 4.2 }]
    },
    {
        name: "series2",
        values: [{ x: new Date(), y: 3.2 }, { x: new Date(), y: 11.2 }]
    }
];

var barData = [
    { label: 'A', value: 5 },
    { label: 'B', value: 6 },
    { label: 'F', value: 7 }
];

var pieData = [
  { label: 'Margarita', value: 20.0 },
  { label: 'John', value: 55.0 },
  { label: 'Tim', value: 25.0 }
];

var treemapData = [
    { label: "China", value: 1364 },
    { label: "India", value: 1296 },
    { label: "Brazil", value: 203 }
];

// Chart types
var BarChart = rd3.BarChart;
var LineChart = rd3.LineChart;
var PieChart = rd3.PieChart;
var AreaChart = rd3.AreaChart;
var Treemap = rd3.Treemap;
var ScatterChart = rd3.ScatterChart;
var CandleStickChart = rd3.CandleStickChart;

var MyLineChart = React.createElement(LineChart, {
    displayName: "LineChart",
        legend: true,
        data: lineData,
        width: 500,
        height: 300,
        title: "Line Chart"
    }
);

var MyScatterChart = React.createElement(ScatterChart, {
    data: scatterData,
    width: 500,
    height: 400,
    yHideOrigin: true,
    title: "Scatter Chart"
});

var MyAreaChart = React.createElement(AreaChart, {
        data: areaData,
        width: 400,
        height: 300,
        xAxisTickInterval: { unit: 'year', interval: 2 },
        title: "Area Chart"
    }
);

var MyBarChart = React.createElement(BarChart, {
        data: barData,
        width: 500,
        height: 200,
        fill: '#3182bd',
        title: "Bar Chart"
    }
);

var MyPieChart = React.createElement(PieChart, {
        data: pieData,
        width: 400,
        height: 400,
        radius: 100,
        innerRadius: 20,
        title: "Pie Chart"
    }
);

var MyTreeMap = React.createElement(Treemap, {
        data: treemapData,
        width: 450,
        height: 250,
        textColor: "#484848",
        fontSize: "10px",
        title: "Treemap"
    }
);

var MyWrapper = React.createClass({
    render: function() {
        return React.createElement("div", null, MyLineChart, MyScatterChart, MyAreaChart, MyBarChart, MyPieChart, MyTreeMap);
    }
});

module.exports = MyWrapper;