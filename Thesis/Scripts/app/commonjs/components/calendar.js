﻿var React = require('react');

var Calendar = React.createClass({
    displayName: "Calendar",
    render: function () {
        return (
            React.createElement("div", null, "Calendar!", this.props.name)
        );
    }
});

module.exports = Calendar;