﻿var React = require('react');
//var Table = require('fixed-data-table');
var Reactable = require('reactable');

var Table = Reactable.Table;
var MyTable = React.createElement(Table, {
    className: "table table-striped",
    id: "table1",
    data: [
        { Name: 'Griffin Smith', Age: '18' },
        { Age: '23', Name: 'Lee Salminen' },
        { Age: '28', Position: 'Developer' },
        { Name: 'Griffin Smith', Age: '18' },
        { Age: '30', Name: 'Test Person' },
        { Name: 'Another Test', Age: '26', Position: 'Developer' },
        { Name: 'Third Test', Age: '19', Position: 'Salesperson' },
        { Age: '23', Name: 'End of this Page', Position: 'CEO' }
    ],
    sortable: [
        {
            column: 'Name',
            sortFunction: function(a, b) {
                var nameA = a.split(' ');
                var nameB = b.split(' ');

                return nameA[1].localeCompare(nameB[1]);
            }
        },
        'Age',
        'Name'
    ],
    filterable: ['Name', 'Age']
});

var MyClass = React.createClass({
    displayName: "Tablichka",
    render: function() {
        return MyTable;
    }
});

module.exports = MyClass;