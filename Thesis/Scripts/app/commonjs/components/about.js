﻿var React = require('react');

var About = React.createClass({
    displayName: "About",
    render: function () {
        return (
            React.createElement("div", null, "About!", this.props.name)
        );
    }
});

module.exports = About;