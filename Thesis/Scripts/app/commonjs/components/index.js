﻿var React = require('react');

var Index = React.createClass({
    displayName: "index",
    render: function () {
        return (
            React.createElement("div", null, "Index!", this.props.name)
        );
    }
});

module.exports = Index;