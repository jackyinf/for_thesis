﻿var React = require('react');

var Performance = React.createClass({
    displayName: "Performance",
    render: function () {
        return (
            React.createElement("div", null, "Performance!", this.props.name)
        );
    }
});

module.exports = Performance;