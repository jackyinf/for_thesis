﻿var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    browserify = require('gulp-browserify'),
    watch = require('gulp-watch');

gulp.task('scripts', function () {
    gulp.src('main.js')
        .pipe(browserify({
            insertGlobals: false,
            debug: false
        })).pipe(gulp.dest('./build'));
});

gulp.task('minify', function () {
    gulp.src('./build/main.js')
        .pipe(uglify({
            outSourceMap: true
        })).pipe(gulp.dest('./dist'));
});

var tasksToRun = ['scripts'];
//if (process.env.NODE_ENV === 'Release') {
//    tasksToRun.push('minify');
//}

gulp.task('watch', function () {
    watch("./**/*.js", function () {
        gulp.start('scripts');
    });

    watch("*.js", function () {
        gulp.start('scripts');
    });
});

gulp.task('default', tasksToRun);