﻿'use strict';
//var common = require('./common.js'); 
//var React = require('../../vendor/react/react-0.13.1.js');
//var React = require('../../vendor/react/react-with-addons-0.13.1.js');
var React = require('react');

//var flux = require('../../vendor/flux/Flux.js');
var Router = require('react-router');

var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var Index = require('./components/index.js');
var MyTable = require('./components/table.js');
var Charts = require('./components/charts.js');
var Calendar = require('./components/calendar.js');
var Performance = require('./components/performance.js');
var About = require('./components/about.js');

//React.render(React.createElement(MyTable, null), document.getElementById('table'));

//React.render(Charts.MyLineChart, document.getElementById('chart1'));
//React.render(Charts.MyScatterChart, document.getElementById('chart2'));
//React.render(Charts.MyAreaChart, document.getElementById('chart3'));
//React.render(Charts.MyBarChart, document.getElementById('chart4'));
//React.render(Charts.MyPieChart, document.getElementById('chart5'));
//React.render(Charts.MyTreeMap, document.getElementById('chart6'));

//React.render(Calendar, document.getElementById('calendar'));
//React.render(Performance, document.getElementById('performance'));
//React.render(React.createElement(About, null), document.getElementById('about'));

 //=========================================
 //Router

var App = React.createClass({
    displayName: "App",
    render: function () {
        return (
          React.createElement("div", null,
              React.createElement("div", { className: "btn-group btn-group-justified", role: "group" },
                React.createElement(Link, { to: "index", className: "btn btn-default" }, "Index"),
                React.createElement(Link, { to: "table", className: "btn btn-default" }, "Table"),
                React.createElement(Link, { to: "charts", className: "btn btn-default" }, "Charts"),
                React.createElement(Link, { to: "calendar", className: "btn btn-default" }, "Calendar"),
                React.createElement(Link, { to: "performance", className: "btn btn-default" }, "Performance"),
                React.createElement(Link, { to: "about", className: "btn btn-default" }, "About")
            ),

            /* this is the important part */
            React.createElement(RouteHandler, null)
          )
        );
    }
});

var routes = (
  React.createElement(Route, { handler: App, path: "/Spas/Commonjs" },
    React.createElement(DefaultRoute, { name: "index", handler: Index }),
    React.createElement(Route, { name: "table", handler: MyTable }),
    React.createElement(Route, { name: "charts", handler: Charts }),
    React.createElement(Route, { name: "calendar", handler: Calendar }),
    React.createElement(Route, { name: "performance", handler: Performance }),
    React.createElement(Route, { name: "about", handler: About })
  )
);

//Router.run(routes, function (Handler) {
//    React.render(React.createElement(Handler, null), document.getElementById('index'));
//});

// Or, if you'd like to use the HTML5 history API for cleaner URLs:

Router.run(routes, Router.HistoryLocation, function (Handler) {
    React.render(React.createElement(Handler, null), document.getElementById('index'));
});