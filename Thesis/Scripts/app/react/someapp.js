﻿define(["someplugin", "react", "reactable"], function (someplugin, React, Reactable) {
    return {
        init: function() {
            console.log("INIT");
            someplugin.hoho();
            console.log(React);

            // Hello world
            var HelloWorld = React.createClass({
                displayName: "HelloWorld",
                render: function () {
                    return (
                        React.createElement("div", null, "Hello ", this.props.name)
                    );
                }
            });

            React.render(React.createElement(HelloWorld, null), document.getElementById('reactapp'));

            // Table
            var Table = Reactable.Table;

            $(function () {
                $.get('/api/TestModels?count=1000').done(function (data) {

                    var table1 = React.createElement(Table, {
                        className: "table table-striped",
                        id: "table1",
                        data: data,
                        sortable: [
                            {
                                column: 'PropertyThree'
                            },
                            'PropertyFour',
                            'PropertyFive'
                        ],
                        //defaultSort: { column: 'PropertyOne', direction: 'desc' },
                        filterable: ['PropertyOne', 'PropertyTwo']
                    });

                    React.render(table1, document.getElementById('table1'));
                });
            });

            // Charts

            var data = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };

            //var chart1 = BarChart.createClass('Bar', {}, data);
            //var chart0 = React.createClass({
            //    displayName: "MyComponent",
            //    render: function () {
            //        return React.createElement(BarChart, { data: data });
            //    }
            //});

            //React.render(React.createElement(chart1, null), document.getElementById('chart0'));
        }
    }
});