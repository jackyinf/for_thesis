﻿var HelloWorld = React.createClass({
    displayName: "HelloWorld",
    render: function () {
        return (
            React.createElement("div", null, "Hello ", this.props.name)
        );
    }
});

//React.render(React.createElement(HelloWorld, null), document.getElementById('reactapp'));

// Table


var Table = Reactable.Table;

// Won't work :(
var table1 = React.createElement(Table, {
    className: "table table-striped",
    id: "table1",
    //data: data,
    sortable: [
        {
            column: 'PropertyThree'
        },
        'PropertyFour',
        'PropertyFive'
    ],
    //defaultSort: { column: 'PropertyOne', direction: 'desc' },
    filterable: ['PropertyOne', 'PropertyTwo']
});

$(function() {
    $.get('/api/TestModels?count=1000').done(function (data) {
        table1.data = data;
        //React.render(table1, document.getElementById('table1'));
    });
});

// Charts

var BarChart = reactChartCore.createClass('Bar', ['getBarsAtEvent']);


var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.5)",
            strokeColor: "rgba(151,187,205,0.8)",
            highlightFill: "rgba(151,187,205,0.75)",
            highlightStroke: "rgba(151,187,205,1)",
            data: [28, 48, 40, 19, 86, 27, 90]
        }
    ]
};

var chart0 = React.createClass({
    displayName: "MyComponent",
    render: function () {
        return React.createElement(BarChart, { data: data });
    }
});

//React.render(React.createElement(chart0, null), document.getElementById('chart0'));

// Index

var Index = React.createClass({
    render: function() {
        return React.createElement("h1", null, "I am index");
    }
});

// Calendar

var Calendar = React.createClass({
    render: function() {
        return React.createElement("div", null,
            React.createElement("h1", null, "hello calendar"));
    }
});

// Performance

var Performance = React.createClass({
    render: function() {
        return React.createElement("div", null,
            React.createElement("h1", null, "hello performance test"));
    }
});

// About

var About = React.createClass({
    render: function () {
        return React.createElement("div", null,
            React.createElement("h1", null, "hello performance test"));
    }
});

// Routing

var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var App = React.createClass({
    displayName: "App",
    render: function() {
        return (
            React.createElement("div", null,
                React.createElement("header", null,
                    React.createElement("ul", null,
                        React.createElement("li", null, React.createElement(Link, { to: "index" }, "Index")),
                        React.createElement("li", null, React.createElement(Link, { to: "table" }, "Table")),
                        React.createElement("li", null, React.createElement(Link, { to: "charts" }, "Charts")),
                        React.createElement("li", null, React.createElement(Link, { to: "calendar" }, "Calendar")),
                        React.createElement("li", null, React.createElement(Link, { to: "performance" }, "Performance")),
                        React.createElement("li", null, React.createElement(Link, { to: "about" }, "About"))
                    ),
                    "Logged in as Jane"
                ),

                /* this is the important part */
                React.createElement(RouteHandler, null)
            )
        );
    }
});

var routes = (
    React.createElement(Route, { name: "app", path: "/Spas/React", handler: App },
        React.createElement(DefaultRoute, { name: "index", handler: Index }),
        React.createElement(Route, { name: "table", handler: table1 }),
        React.createElement(Route, { name: "charts", handler: chart0 }),
        React.createElement(Route, { name: "calendar", handler: Calendar }),
        React.createElement(Route, { name: "performance", handler: Performance}),
        React.createElement(Route, { name: "about", handler: About})
    )
);

Router.run(routes, function (Handler) {
    React.render(React.createElement(Handler, null), document.getElementById('reactapp'));
});