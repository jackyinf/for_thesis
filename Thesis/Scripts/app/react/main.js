﻿require.config({
    baseUrl: '/Scripts/app/react/',
    paths: {
        "hoho": "someplugin.js",
        "chart": "../../vendor/chartjs/chart",
        "react": "../../vendor/react/react-with-addons-0.13.1",
        "reactable": "../../vendor/react/plugins/reactable"
        //"bar": "../../vendor/react/plugins/core"
    }
});

require(["someapp"], function(app) {
    app.init();
});