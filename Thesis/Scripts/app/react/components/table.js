﻿(function() {
    'use strict';

    var Table = Reactable.Table;

    $(function () {

        $.get('/api/TestModels?count=1000').done(function (data) {
            var table1 = React.createElement(Table, {
                className: "table table-striped",
                id: "table1",
                data: data,
                sortable: [
                    {
                        column: 'Name',
                        sortFunction: function (a, b) {
                            // Sort by last name
                            var nameA = a.split(' ');
                            var nameB = b.split(' ');

                            return nameA[1].localeCompare(nameB[1]);
                        }
                    },
                    'Age',
                    'Position'
                ],
                //defaultSort: { column: 'PropertyOne', direction: 'desc' },
                filterable: ['PropertyOne', 'PropertyOne']
            });

            React.render(table1, document.getElementById('table1'));
        });

    });

})();