﻿(function() {
    //'use strict';

    var routes = (
        React.createElement(Route, { handler: App, path: "/" },
            React.createElement(DefaultRoute, { handler: Home }),
            React.createElement(Route, { name: "about", handler: About })
        )
    );

    Router.run(routes, function(Handler) {
        React.render(React.createElement(Handler, null), document.body);
    });


})();