﻿var Song = Backbone.Model.extend({
    initialize: function(obj) {
        console.log('music is created with title ' + obj.title);
    }
});

// Collection

var Album = Backbone.Collection.extend({
    model: Song
});

var song1 = new Song({ title: 'Michael Jackson' });
song1.set({ title: 'Beat It' });
console.log(song1.get("title"));

var album = new Album();
album.add(song1);
console.log(album.length);
console.log(album.pop());
console.log(album.length);


var stooges = new Backbone.Collection([
    { age: 12, name: "Curly"},
    { age: 9, name: "Moe"},
    { age: 33, name: "Larry"}
]);

stooges.comparator = "age";
var sortedStooges = stooges.sortBy("age");
//var names = sortedStooges.pluck("name");
console.log(JSON.stringify(sortedStooges));

// Events

$(function() {

    var myEvent = {};
    var myEvent1 = {};
    _.extend(myEvent, Backbone.Events);
    _.extend(myEvent1, Backbone.Events);

    var btn1 = $("#btn1");

    btn1.on("click", function() {
        myEvent.trigger("alert1", "an event");
    });

    myEvent1.listenTo(myEvent, "alert1", function () {
        console.log("listening");
        myEvent1.stopListening();
    });

    myEvent.on("alert1", function (msg) {
        console.log("triggered event " + msg);
    });

});