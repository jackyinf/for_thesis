﻿App.Router.map(function () {
    this.route("index");
    this.route("table", { path: "/" });
    this.route("charts", { path: "/charts" });
    this.route("calendar");
    this.route("performance", { path: "perf" });
    this.route("about");
});