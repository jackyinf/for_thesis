﻿(function () {
    'use strict';

    Ember.TableView = Ember.View.extend({
        templateName: 'table'
    });
})();