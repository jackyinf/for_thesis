﻿(function() {
    'use strict';

    Ember.PerformanceView = Ember.View.extend({
        templateName: 'performance'
    });

    App.MyInputComponent = Ember.TextField.extend({
        focusOnInsert: function() {
            this.$().val(this.$().val());
            this.$().focus();
        }.on('didInsertElement')
    });

    App.EmailObject = Ember.Object.extend({});

    var email = App.EmailObject.create({ name: "John" });
    console.log(email.get("name"));

})();