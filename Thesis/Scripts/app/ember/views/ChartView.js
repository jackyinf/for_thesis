﻿(function() {
    'use strict';

    Ember.ChartView = Ember.View.extend({
        templateName: 'charts'
    });
})();