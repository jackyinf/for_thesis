﻿window.App = Em.Application.create({
    rootElement: '#emberapp'
});
App.ApplicationAdapter = DS.WebAPIAdapter.extend({
    namespace: 'api',
    antiForgeryTokenSelector: "#antiForgeryToken"
});
