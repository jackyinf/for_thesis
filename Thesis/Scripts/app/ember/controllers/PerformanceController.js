﻿(function() {
    'use strict';

    App.PerformanceController = Ember.Controller.extend({
        model: [],
        message: 'hey',

        actions: {
            addItems: function () {
                var arr = [];
                for (var i = 0; i < 5000; i++) {
                    arr.push({ id: i, message: this.message });
                }
                this.set("model", arr);
            }
        }
    });
})();