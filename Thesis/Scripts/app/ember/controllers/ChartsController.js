﻿(function() {
    'use strict';

    App.ScrubberComponent = Ember.Component.extend({
        tagName: 'input',
        attributeBindings: ['min', 'max', 'step', 'type'],
        type: 'range',
        min: 0,
        max: 10,
        step: 1,
        change: function() {
            var value;
            value = this.$()[0].value;
            return this.set('value', +value);
        }
    });

    App.SlideController = Ember.Controller.extend({
        prettyPrintedData: Ember.computed(function() {
            return JSON.stringify(this.get('data'), null, '\t');
        })
        //}.property('data'), {
    }, {
        seedColors: {
            purple: 'rgb(100,60,120)',
            yellow: 'rgb(250,165,30)',
            maroon: 'rgb(150,0,35)',
            red: 'rgb(235,35,35)',
            blue: 'rgb(30,120,190)',
            navy: 'rgb(25,75,120)',
            green: 'rgb(60,110,80)',
            gray: 'rgb(65,65,65)',
            black: 'rgb(00,00,00)'
        },
        seedColorNames: Ember.computed(function() {
            return _.keys(this.get('seedColors'));
        }).property('seedColors'),
        selectedSeedColorName: 'black',
        selectedSeedColor: Ember.computed(function() {
            return this.get('seedColors')[this.get('selectedSeedColorName')];
        }).property('selectedSeedColorName', 'seedColors.@each')
    });

    App.ChartsController = App.SlideController.extend({
        betweenGroupPadding: 0,
        withinGroupPadding: 0,
        maxLabelHeight: 40,
        stackBars: false,
        availableDataSets: Ember.computed(function() {
            return _.keys(this.get('rawDataHash'));
        }).property('rawDataHash'),
        data: Ember.computed(function() {
            return this.get('rawDataHash')[this.get('selectedData')];
        }).property('selectedData', 'rawDataHash'),
        rawDataHash: Ember.computed(function() {
            return {
                //two_ranges: App.data.two_ranges,
                //three_ranges: App.data.three_ranges,
                //five_ranges: App.data.five_ranges,
                //sector_compare_return: App.data.sector_compare_return,
                ////'----': App.data["null"],
                //asset_values: App.data.asset_values,
                //many_values: App.data.many_values,
                //monthly_return_single_period: App.data.monthly_return_single_period,
                //high_net_worth_duration: App.data.high_net_worth_duration,
                //'----': App.data["null"],
                //empty: App.data.empty,
                //one_value: App.data.one_value,
                //two_values: App.data.two_values,
                //zero: App.data.zero,
                //zeroes: App.data.zeroes,
                //sum_to_zero: App.data.sum_to_zero,
                //bad_range: App.data.bad_range
            
            };
        }),
        selectedData: 'three_ranges'
    });
})();