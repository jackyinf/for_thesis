﻿(function() {
    "use strict";

    angular.module("app").controller('performance.controller', performanceController);

    performanceController.$inject = ['$scope'];

    function performanceController($scope) {
        $scope.list = [];
        $scope.performanceTests = {
            first:  { start: null, end: null, elapsed: null },  // test 0
            second: { start: null, end: null, elapsed: null },  // test 1
            third: { start: null, end: null, elapsed: null },  // test 2
            fourth: { start: null, end: null, elapsed: null },  // test 3
            fifth: { start: null, end: null, elapsed: null }   // test 4
        };

        $scope.addItems = function () {
            $scope.list.length = 0;
            $scope.performanceTests.first.start = moment();
            for (var i = 0; i < 5000; i++) {
                $scope.list.push(i);
            }
            $scope.performanceTests.first.end = moment();
            console.log($scope.performanceTests);
            setElapsedTime($scope.performanceTests.first);
        }

        $scope.addDigestedItems = function(count) {
            $scope.list.length = 0;
            if (!count)
                return;
            for (var i = 0; i < count; i++) {
                $scope.list.push(i);
                $scope.$digest();
            }
        }

        /////////////////////////////

        function setElapsedTime(obj) {
            obj.elapsed = (obj.end - obj.start) + "ms";
        }
    }
})();