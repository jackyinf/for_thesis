﻿(function () {
    "use strict";

    angular.module("app").controller('table.controller', tableController);

    tableController.$inject = ['$scope', '$http'];

    function tableController($scope, $http) {
        activate($scope, $http);
    }

    function activate($scope, $http) {
        $http.get("/api/TestModels?count=1000").success(function (data) {
            $scope.items = data;
        });
    }
})();