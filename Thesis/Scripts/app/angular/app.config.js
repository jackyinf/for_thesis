﻿(function() {
    "use strict";

    angular.module("app").config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");

        var templatePrefix = "/Spas/Partial?name=";
        $stateProvider
            .state("index", {
                url: "/",
                template: "Welcome!"
            })
            .state("table", {
                url: "/table",
                templateUrl: templatePrefix + "table",
                controller: "table.controller"
            })
            .state("charts", {
                url: "/charts",
                templateUrl: templatePrefix + "charts",
                controller: "charts.controller"
            })
            .state("calendar", {
                url: "/calendar",
                templateUrl: templatePrefix + "calendar",
                controller: "calendar.controller"
            })
            .state("performance", {
                url: "/performance",
                templateUrl: templatePrefix + "performance",
                controller: "performance.controller"
            })
        .state("error", {
            url: "/error",
            template: "404 error occured: view not found"
        });
    });

})();