﻿(function() {
    'use strict';

    angular.module("app", ['ui.grid', 'ui.bootstrap', 'toastr', 'ui.calendar', 'chart.js', 'ui.router', 'angularMoment']);
})();