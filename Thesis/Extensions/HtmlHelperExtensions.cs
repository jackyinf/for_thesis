﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Thesis.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString RequireJs(this HtmlHelper helper, string common, string module)
        {
            var require = new StringBuilder();

            const string jsLocation = "Scripts/";
            require.AppendLine("<script>");
            require.AppendLine(string.Format(@"require( [ ""/{0}{1}.js"" ], function() {{", jsLocation, common));
            require.AppendLine(string.Format("  require([\"{0}\"]);", module));
            require.AppendLine("});");
            require.AppendLine("</script>");

            return new MvcHtmlString(require.ToString());
        }
    }
    
}