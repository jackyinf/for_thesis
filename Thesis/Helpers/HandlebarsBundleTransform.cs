﻿using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using Ember.Handlebars;
using Microsoft.Ajax.Utilities;

namespace Thesis.Helpers
{
    public class HandlebarsBundleTransform : IBundleTransform
    {
        private bool minifyTemplates = true;
        private object _server;
        private const string PathToEmberTemplates = @"~/Scripts/app/ember/templates/";
            // System.IO.File.ReadAllText();

        public bool MinifyTemplates
        {
            get
            {
                return this.minifyTemplates;
            }
            set
            {
                this.minifyTemplates = value;
            }
        }

        public void Process(BundleContext context, BundleResponse response)
        {
            TemplateBuilder templateBuilder = new TemplateBuilder();
            foreach (var file in response.Files)
            {
                //string templateBody = File.ReadAllText(file.VirtualFile.Name);
                string content = File.ReadAllText(HostingEnvironment.MapPath(PathToEmberTemplates + file.VirtualFile.Name));
                MvcHtmlString templateBody = new MvcHtmlString("<script type=\"text/x-handlebars\" data-template-name=\"" + file.VirtualFile.Name + "\">\n" + content + "\n</script>\n");
                string withoutExtension = Path.GetFileNameWithoutExtension(file.VirtualFile.Name);
                templateBuilder.Register(withoutExtension, templateBody.ToHtmlString());
            }

            string str1 = templateBuilder.ToString();
            if (this.minifyTemplates)
            {
                Minifier minifier = new Minifier();
                string str2 = minifier.MinifyJavaScript(templateBuilder.ToString());
                if (minifier.ErrorList.Count <= 0)
                    str1 = str2;
            }
            response.ContentType = "text/javascript";
            response.Cacheability = HttpCacheability.Public;
            response.Content = str1;
        }
    }
}

