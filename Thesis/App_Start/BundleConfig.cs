﻿using System.Web.Optimization;
using System.Web.Optimization.React;

namespace Thesis
{
    public static class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // jQuery plugins
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/vendor/jquery/jquery-2.1.1.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include("~/Scripts/vendor/jquery/jquery-ui-1.10.4.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery-mousewheel").Include("~/Scripts/vendor/jquery/jquery.mousewheel.js"));

            // Antiscroll
            bundles.Add(new ScriptBundle("~/Content/antiscroll").Include("~/Content/vendor/antiscroll.css"));
            bundles.Add(new ScriptBundle("~/bundles/antiscroll").Include(
                "~/Scripts/vendor/antiscroll/scrollbar.js",
                "~/Scripts/vendor/antiscroll/antiscroll.js"
            ));

            // D3.js and Chart.js
            bundles.Add(new ScriptBundle("~/bundles/d3").Include("~/Scripts/vendor/d3/d3.js"));
            bundles.Add(new ScriptBundle("~/bundles/chartjs").Include("~/Scripts/vendor/chartjs/chart.js"));  

            // Lodash
            bundles.Add(new ScriptBundle("~/bundles/lodash").Include("~/Scripts/vendor/lodash/lodash.js"));

            // jQuery validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/vendor/jquery/jquery.unobtrusive-ajax.js",
                "~/Scripts/vendor/jquery/jquery.validate.js"
            ));

            // Modernizr
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/vendor/modernizr/modernizr-2.6.2.js"));

            // Bootstrap + Respond
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/vendor/bootstrap/bootstrap.js",
                      "~/Scripts/vendor/respond/respond.js"));

            // Main CSS content
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/vendor/bootstrap.css",
                      "~/Content/vendor/site.css"));
            
            // Moment
            bundles.Add(new ScriptBundle("~/bundles/moment").Include("~/Scripts/vendor/moment/moment.js"));  

            // Full calendar
            bundles.Add(new ScriptBundle("~/bundles/fullcalendar").Include(
                "~/Scripts/vendor/fullcalendar/fullcalendar.js",
                "~/Scripts/vendor/fullcalendar/gcal.js"
            ));

            bundles.Add(new StyleBundle("~/Content/fullcalendar").Include(
                "~/Content/vendor/fullcalendar/fullcalendar.css",
                "~/Content/vendor/fullcalendar/fullcalendar/print.css"
            ));

            // Traceur - ES6 to ES5
            bundles.Add(new ScriptBundle("~/bundles/traceur").Include("~/Scripts/vendor/traceur/traceur.js"));

            // Underscore
            bundles.Add(new ScriptBundle("~/bundles/underscore").Include("~/Scripts/vendor/underscore/underscore.js"));

            // RequireJS
            bundles.Add(new ScriptBundle("~/bundles/requirejs").Include("~/Scripts/vendor/requirejs/require.js"));

            // ===========================================
            // ===========================================
            // ================= Angular =================
            // ===========================================
            // ===========================================

            bundles.Add(new StyleBundle("~/Content/angular").Include(
                "~/Content/vendor/angular/ui-grid-stable.css",
                "~/Content/vendor/angular/angular-chart.css",
                "~/Content/vendor/angular/angular-toastr.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/vendor/angular/angular.js",
                "~/Scripts/vendor/angular/angular-animate.js",
                "~/Scripts/vendor/angular/angular-aria.js",
                "~/Scripts/vendor/angular/angular-cookies.js",
                "~/Scripts/vendor/angular/angular-google-map.js",
                "~/Scripts/vendor/angular/angular-loader.js",
                "~/Scripts/vendor/angular/angular-message.js",
                "~/Scripts/vendor/angular/angular-scenario.js",
                "~/Scripts/vendor/angular/angular-sanitize.js",
                "~/Scripts/vendor/angular/angular-touch.js",
                "~/Scripts/vendor/angular/angular-ui-router.js",
                "~/Scripts/vendor/angular/angular-ui/ui-bootstrap-tpls.js",
                "~/Scripts/vendor/angular/ui-grid/ui-grid-stable.js",
                "~/Scripts/vendor/angular/angular-chart.js",
                "~/Scripts/vendor/angular/angular-toastr.tpls.js",
                "~/Scripts/vendor/angular/calendar.js",
                "~/Scripts/vendor/angular/moment/angular-moment.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/angular-min").Include(
                "~/Scripts/vendor/angular/angular.min.js",
                "~/Scripts/vendor/angular/angular-animate.min.js",
                "~/Scripts/vendor/angular/angular-aria.min.js",
                "~/Scripts/vendor/angular/angular-cookies.min.js",
                "~/Scripts/vendor/angular/angular-google-maps.min.js",
                "~/Scripts/vendor/angular/angular-loader.min.js",
                "~/Scripts/vendor/angular/angular-message.min.js",
                //"~/Scripts/vendor/angular/angular-scenario.js",
                "~/Scripts/vendor/angular/angular-sanitize.min.js",
                "~/Scripts/vendor/angular/angular-touch.min.js",
                "~/Scripts/vendor/angular/angular-ui-router.min.js",
                "~/Scripts/vendor/angular/angular-ui/ui-bootstrap-tpls.min.js",
                "~/Scripts/vendor/angular/ui-grid/ui-grid-stable.min.js",
                "~/Scripts/vendor/angular/angular-chart.min.js",
                "~/Scripts/vendor/angular/angular-toastr.tpls.min.js",
                "~/Scripts/vendor/angular/calendar.js",
                "~/Scripts/vendor/angular/moment/angular-moment.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/angularapp")
                .Include("~/Scripts/app/angular/app.js")
                .Include("~/Scripts/app/angular/app.config.js")
                .IncludeDirectory("~/Scripts/app/angular/services", "*.js")
                .IncludeDirectory("~/Scripts/app/angular/controllers", "*.js")
            );

            // ===========================================
            // ===========================================
            // ================= Ember ===================
            // ===========================================
            // ===========================================

            bundles.Add(new StyleBundle("~/Content/ember").Include());
 
            bundles.Add(new ScriptBundle("~/bundles/ember").Include(
                "~/scripts/vendor/handlebars/handlebars.js",
                "~/scripts/vendor/ember/ember.js",
                "~/scripts/vendor/ember/ember-data.js",
                "~/Scripts/app/ember/webapi_serializer.js",
                "~/Scripts/app/ember/webapi_adapter.js"
            ));

            bundles.Add(new StyleBundle("~/Content/ember.min").Include());
            bundles.Add(new ScriptBundle("~/bundles/ember.min").Include());

            bundles.Add(new ScriptBundle("~/bundles/emberapp")
                .IncludeDirectory("~/Scripts/vendor/ember/plugins", "*.js")
                .Include(
                 "~/Scripts/app/ember/app.js",
                 "~/Scripts/app/ember/router.js",
                 "~/Scripts/app/ember/helpers.js")
                 //.IncludeDirectory("~/Scripts/app/ember/routes", "*.js")
                 //.IncludeDirectory("~/Scripts/app/ember/models", "*.js")
                  .IncludeDirectory("~/Scripts/app/ember/views", "*.js")
                  .IncludeDirectory("~/Scripts/app/ember/controllers", "*.js")
             );
            bundles.Add(new StyleBundle("~/Content/emberapp").IncludeDirectory("~/Content/vendor/ember", "*.css"));

            // ===========================================
            // ===========================================
            // ================= React ===================
            // ===========================================
            // ===========================================

            bundles.Add(new StyleBundle("~/Content/react").Include());

            bundles.Add(new ScriptBundle("~/bundles/react").Include(
                "~/Scripts/vendor/react/react-with-addons-{version}.js",
                "~/Scripts/vendor/react/JSXTransformer-{version}.js"
                ).IncludeDirectory("~/Scripts/vendor/react/plugins", "*.js"));

            bundles.Add(new JsxBundle("~/bundles/reactappjsx").Include("~/Scripts/app/react/app.jsx"));
            bundles.Add(new ScriptBundle("~/bundles/reactapp").Include("~/Scripts/app/react/app.js"));

            // ===========================================
            // ===========================================
            // ================= Backbone ================
            // ===========================================
            // ===========================================

            bundles.Add(new ScriptBundle("~/bundles/backbone").Include("~/Scripts/vendor/backbone/backbone.js"));
            bundles.Add(new ScriptBundle("~/bundles/backboneapp").Include("~/Scripts/app/backbone/app.js"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
