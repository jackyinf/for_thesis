﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Thesis.Models;

namespace Thesis.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TestModelsController : ApiController
    {
        private readonly ThesisContext db = new ThesisContext();

        // GET: api/TestModels
        public IQueryable<TestModel> GetTestModels()
        {
            return db.TestModels;
        }

        // GET: api/TestModels/5
        [ResponseType(typeof(TestModel))]
        public IHttpActionResult GetTestModel(int id)
        {
            TestModel testModel = db.TestModels.Find(id);
            if (testModel == null)
            {
                return NotFound();
            }

            return Ok(testModel);
        }

        // PUT: api/TestModels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTestModel(int id, TestModel testModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != testModel.ID)
            {
                return BadRequest();
            }

            db.Entry(testModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TestModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TestModels
        [ResponseType(typeof(TestModel))]
        public IHttpActionResult PostTestModel(TestModel testModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TestModels.Add(testModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = testModel.ID }, testModel);
        }

        // DELETE: api/TestModels/5
        [ResponseType(typeof(TestModel))]
        public IHttpActionResult DeleteTestModel(int id)
        {
            TestModel testModel = db.TestModels.Find(id);
            if (testModel == null)
            {
                return NotFound();
            }

            db.TestModels.Remove(testModel);
            db.SaveChanges();

            return Ok(testModel);
        }

        public List<TestModel> GetSampleData(int count)
        {
            var items = new List<TestModel>();
            for (int i = 0; i < count; i++)
                items.Add(new TestModel());
            return items;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TestModelExists(int id)
        {
            return db.TestModels.Count(e => e.ID == id) > 0;
        }
    }
}