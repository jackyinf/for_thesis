﻿using System.Web.Mvc;

namespace Thesis.Controllers
{
    public class SpasController : Controller
    {
        public ActionResult Angular()
        {
            return View();
        }

        public ActionResult Ember()
        {
            return View();
        }

        public ActionResult React()
        {
            return View();
        }

        public ActionResult Backbone()
        {
            return View();
        }

        public ActionResult Commonjs()
        {
            return View();
        }

        public PartialViewResult Partial(string name)
        {
            return PartialView("AngularTemplates/" + name);
        }
    }
}